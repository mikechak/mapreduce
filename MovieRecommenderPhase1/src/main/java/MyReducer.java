import java.io.*;
import java.util.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;

public class MyReducer extends Reducer<Text,Text,NullWritable,Text> {
	private Text output = new Text();
	private Integer size = 0;
	
	public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
		List<Text> cache = new ArrayList<Text>();
		size = 0;
		
		for (Text val : values) {
			size++;
			cache.add(new Text(val));
		}
		
		for (Text val : cache) {
			//List<String> lst = new ArrayList<String>(Arrays.asList(val.toString().split(",")));
			String[] arr = val.toString().split(",");
			
			//output.set(lst.get(0) + "," + key + "," + lst.get(1) + "," + size.toString());
			output.set(arr[0] + "," + key + "," + arr[1] + "," + size.toString());
			context.write(NullWritable.get(), output);
		}
	}
}
