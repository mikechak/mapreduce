import java.io.*;
import java.util.*;

import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;

public class MyMapper extends Mapper<LongWritable,Text,Text,Text> {
	private Text movie = new Text();
	private Text val = new Text();
	
	public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
		List<String> lst = new ArrayList<String>(Arrays.asList(value.toString().split(",")));
		if (lst.size() == 3) {
			movie.set(lst.get(1));
			val.set(lst.get(0) + "," + lst.get(2));
			context.write(movie, val);
		}
	}
}
