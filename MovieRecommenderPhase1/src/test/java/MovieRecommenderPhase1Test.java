import java.io.*;
import java.util.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mrunit.mapreduce.*;
import org.junit.Before;
import org.junit.Test;

public class MovieRecommenderPhase1Test {
	MapDriver<LongWritable,Text,Text,Text> mapDriver;
	ReduceDriver<Text,Text,NullWritable,Text> reduceDriver;
	
	@Before
	public void setup() {
		MyMapper mapper = new MyMapper();
		MyReducer reducer = new MyReducer();
		
		mapDriver = MapDriver.newMapDriver(mapper);
		reduceDriver = ReduceDriver.newReduceDriver(reducer);
	}
	
	@Test
	public void testMapper() throws IOException {
		mapDriver.withInput(new LongWritable(), new Text("User1,Movie1,1"));
		mapDriver.withOutput(new Text("Movie1"), new Text("User1,1"));
		mapDriver.runTest();
	}
	
	@Test
	public void testReducer() throws IOException {
		List<Text> values = new ArrayList<Text>();
		values.add(new Text("User1,1"));
		values.add(new Text("User2,1"));
		
		reduceDriver.withInput(new Text("Movie1"), values);
		
		reduceDriver.withOutput(NullWritable.get(), new Text("User1,Movie1,1,2"));
		reduceDriver.withOutput(NullWritable.get(), new Text("User2,Movie1,1,2"));
		reduceDriver.runTest();
	}
}
