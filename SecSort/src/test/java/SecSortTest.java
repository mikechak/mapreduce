import org.apache.hadoop.mrunit.mapreduce.*;
import org.junit.Before;
import org.junit.Test;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.io.*;
import java.io.*;
import java.util.*;

public class SecSortTest {
	MapDriver<Text,Text,ComKey,Text> mapDriver;
	ReduceDriver<ComKey,Text,NullWritable,Text> reduceDriver;
	MapReduceDriver<Text,Text,ComKey,Text,NullWritable,Text> mapreduceDriver;
	
	@Before
	public void setup() {
		MyMapper mapper = new MyMapper();
		MyReducer reducer = new MyReducer();
		
		mapDriver = MapDriver.newMapDriver(mapper);
		reduceDriver = ReduceDriver.newReduceDriver(reducer);
		mapreduceDriver = MapReduceDriver.newMapReduceDriver(mapper, reducer);
	}
	
	@Test
	public void testMapper() throws IOException {
		mapDriver.withInput(new Text("C"), new Text("C"));
		mapDriver.withInput(new Text("C"), new Text("A"));
		mapDriver.withOutput(new ComKey("C","C"), new Text("C"));
		mapDriver.withOutput(new ComKey("C","A"), new Text("A"));
		
		mapDriver.runTest();
	}
	
	@Test
	public void testReducer() throws IOException {
		List<Text> values = new ArrayList<Text>();
		values.add(new Text("A"));
		values.add(new Text("C"));
		reduceDriver.withInput(new ComKey("C","C"), values);
		reduceDriver.withOutput(NullWritable.get(), new Text("C | A"));
		reduceDriver.withOutput(NullWritable.get(), new Text("C | C"));
		
		reduceDriver.runTest();
	}
	
	@Test
	public void testMapReducer() throws IOException {
		mapreduceDriver.withInput(new Text("C"), new Text("C"));
		mapreduceDriver.withInput(new Text("C"), new Text("A"));
		
		mapreduceDriver.withOutput(NullWritable.get(), new Text("C | A"));
		mapreduceDriver.withOutput(NullWritable.get(), new Text("C | C"));
		
		mapreduceDriver.runTest();
	}
}
