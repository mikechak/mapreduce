import java.io.IOException;

import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;


public class MyReducer extends Reducer<ComKey, Text, NullWritable, Text> {
	Text output = new Text();
	NullWritable empty = NullWritable.get();
	
	public void reduce(ComKey key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
		
		for (Text val : values) {
			output.set(key.getPKey() + " | " + val);
			context.write(empty, output);
		}
	}
}
