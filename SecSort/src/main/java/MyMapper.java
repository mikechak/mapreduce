import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class MyMapper extends Mapper<Text,Text,ComKey,Text> {
	ComKey comKey = new ComKey();
				
	public void map(Text key, Text value, Context context) throws IOException, InterruptedException {
		comKey.set(key, value);
		context.write(comKey, value);
	}
	
}
