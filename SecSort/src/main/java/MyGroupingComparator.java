import org.apache.hadoop.io.*;

public class MyGroupingComparator extends WritableComparator {
	public MyGroupingComparator() {
		super(ComKey.class, true);
	}
	
	@Override
	public int compare(WritableComparable a, WritableComparable b) {
		ComKey key1 = (ComKey)a;
		ComKey key2 = (ComKey)b;
		return key1.getPKey().compareTo(key2.getPKey());
	}
}
