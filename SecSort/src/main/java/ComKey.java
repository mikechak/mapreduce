import java.io.*;

import org.apache.hadoop.io.*;

public class ComKey implements Writable, WritableComparable<ComKey> {
	private Text pKey;
	private Text sKey;
	
	public ComKey() {
		pKey = new Text();
		sKey = new Text();
	}
	
	public ComKey(String pkey, String skey) {
		pKey = new Text(pkey);
		sKey = new Text(skey);
	}
	
	public void set(Text pkey, Text skey) {
		this.pKey = pkey;
		this.sKey = skey;
	}
	
	public Text getPKey() {
		return this.pKey;
	}
	
	public Text getSKey() {
		return this.sKey;
	}
	
	public void write(DataOutput dataOutput) throws IOException {
		pKey.write(dataOutput);
		sKey.write(dataOutput);
	}
	
	public void readFields(DataInput dataInput) throws IOException {
		pKey.readFields(dataInput);
		sKey.readFields(dataInput);
	}
	
	//@Override
	public int compareTo(ComKey obj) {
		int ret = this.pKey.compareTo(obj.getPKey());
		if (ret == 0) {
			return this.sKey.compareTo(obj.getSKey());
		}
		return ret;
	}
	
	@Override
	public int hashCode() {
		return this.pKey.hashCode() + this.sKey.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null || getClass() != obj.getClass()) return false;
		
		ComKey o = (ComKey)obj;
		
		if (!this.pKey.equals(o.getPKey())) return false;
		if (!this.sKey.equals(o.getSKey())) return false;
		
		return true;
	}
}
