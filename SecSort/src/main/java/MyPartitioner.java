import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;

public class MyPartitioner extends Partitioner<ComKey, Text> {
	@Override
	public int getPartition(ComKey key, Text text, int numPartitions) {
		return key.getPKey().hashCode() % numPartitions;
	}
}
