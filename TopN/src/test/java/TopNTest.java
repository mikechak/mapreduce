import java.util.*;
import java.io.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mrunit.mapreduce.*;
import org.junit.Before;
import org.junit.Test;

public class TopNTest {
	MapDriver<LongWritable,Text,NullWritable,Text> mapDriver;
	
	@Before
	public void setup() {
		MyMapper mapper = new MyMapper();
		
		mapDriver = MapDriver.newMapDriver(mapper);
	}
	
	@Test
	public void testMapper() throws IOException {
		Configuration conf = mapDriver.getConfiguration();
		conf.setInt("N", 3);
		
		mapDriver.withInput(new LongWritable(), new Text("1,ABC"));
		mapDriver.withInput(new LongWritable(), new Text("3,ABC"));
		mapDriver.withInput(new LongWritable(), new Text("2,ABC"));
		mapDriver.withInput(new LongWritable(), new Text("7,ABC"));
		mapDriver.withInput(new LongWritable(), new Text("8,ABC"));
		mapDriver.withInput(new LongWritable(), new Text("4,ABC"));
		
		mapDriver.withOutput(NullWritable.get(), new Text("4,ABC"));
		mapDriver.withOutput(NullWritable.get(), new Text("7,ABC"));
		mapDriver.withOutput(NullWritable.get(), new Text("8,ABC"));
		
		mapDriver.runTest();
	}
}
