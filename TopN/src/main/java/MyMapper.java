import java.io.*;
import java.util.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.io.*;
//import com.google.common.base.Splitter;
//import com.google.common.base.Joiner;
//import com.google.common.collect.Lists;

public class MyMapper extends Mapper<LongWritable,Text,NullWritable,Text> {
	private SortedMap<Integer,String> smap = new TreeMap<Integer,String>();
	private int max;
	//private Splitter splitter;
	//private Joiner joiner;
	
	@Override
	public void setup(Context context) throws IOException {
		max = Integer.parseInt(context.getConfiguration().get("N"));
		//splitter = Splitter.on(",").trimResults();
		//joiner = Joiner.on(",");
	}
	
	public void map(LongWritable key,Text value, Context context) throws IOException, InterruptedException {
		//List<String> list = Lists.newArrayList(splitter.split(value.toString()));
		List<String> list = Arrays.asList(value.toString().split(","));
		//smap.put(Integer.parseInt(list.get(0)), joiner.join(list)); 
		smap.put(Integer.parseInt(list.get(0)), StringUtils.join(list,","));
		
		if (smap.size() > max) {
			smap.remove(smap.firstKey());
		}
	}
	
	@Override
	public void cleanup(Context context) throws IOException, InterruptedException {
		for (String str : smap.values()) {
			context.write(NullWritable.get(), new Text(str));
		}
	}
}
