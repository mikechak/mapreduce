import java.io.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;

public class MyMapper extends Mapper<Text,Text,ComKey,Text> {
	
	public void map(Text key, Text value, Context context) throws IOException, InterruptedException {
		String[] lst = key.toString().split(",");
		
		context.write(new ComKey(lst[0],lst[1]), value);
		
		//context.write(new ComKey("",""), new Text(""));
	}

}
