import java.io.*;
import java.util.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;

public class MyReducer extends Reducer<ComKey,Text,Text,Text> {
	private Text combi = new Text();
	private Text correlation = new Text();
	
	public void reduce(ComKey key, Iterable<Text>values, Context context) throws IOException, InterruptedException {
		int groupSize = 0;
		int dotProduct = 0;
		int rating1Sum = 0;
		int rating2Sum = 0;
		int rating1NormSq = 0;
		int rating2NormSq = 0;
		int maxNumOfRaters1 = 0;
		int maxNumOfRaters2 = 0;
		
		for (Text val : values) {
			String[] lst = val.toString().split(",");
			
			groupSize++;
			rating1Sum += Integer.parseInt(lst[0]);
			rating2Sum += Integer.parseInt(lst[2]);
			dotProduct += Integer.parseInt(lst[4]);
			rating1NormSq += Integer.parseInt(lst[5]);
			rating2NormSq += Integer.parseInt(lst[6]);
			if (Integer.parseInt(lst[1]) > maxNumOfRaters1) {
				maxNumOfRaters1 = Integer.parseInt(lst[1]);
			}
			if (Integer.parseInt(lst[3]) > maxNumOfRaters2) {
				maxNumOfRaters1 = Integer.parseInt(lst[3]);
			}
		}
		
		double pearson = calculatePearsonCorrelation(groupSize, dotProduct, rating1Sum, rating2Sum, rating1NormSq, rating2NormSq);
		
		double cosine = calculateCosineCorrelation(dotProduct, Math.sqrt(rating1NormSq), Math.sqrt(rating2NormSq));
		
		double jaccard = calculateJaccardCorrelation(groupSize, maxNumOfRaters1, maxNumOfRaters2);
		
		combi.set(key.getMovieA() + "," + key.getMovieB());
		
		correlation.set(Double.toString(pearson) + "," + Double.toString(cosine) + "," + Double.toString(jaccard));
		
		context.write(combi, correlation);
		
	}
	
	private double calculatePearsonCorrelation(int groupSize, int dotProduct, int rating1Sum, int rating2Sum, int rating1NormSq, int rating2NormSq) {
		return 0.1;
	}
	
	private double calculateCosineCorrelation(int dotProduct, double rating1NormSqrt, double rating2NormSqrt) {
		return 0.2;
	}
	
	private double calculateJaccardCorrelation(int groupSize, int maxNumOfRaters1, int maxNumOfRaters2) {
		return 0.3;
	}

}
