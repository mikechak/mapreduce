import org.apache.hadoop.io.*;
import java.io.*;

public class ComKey implements Writable, WritableComparable<ComKey> {
	private Text movieA;
	private Text movieB;
	//private IntWritable movieAPos;
	//private IntWritable movieBPos;
	
	public ComKey() {
		movieA = new Text();
		movieB = new Text();
		//movieAPos.set(-1);
		//movieBPos.set(-1);
	}
	
	public ComKey(String movieA, String movieB) {
		this();
		this.movieA.set(movieA);
		this.movieB.set(movieB);
	}
	
	public ComKey(String movieA, String movieB, int pos1, int pos2) {
		this();
		this.movieA.set(movieA);
		this.movieB.set(movieB);
		//this.movieAPos.set(pos1);
		//this.movieBPos.set(pos2);
	}
	
	public Text getMovieA() {
		return movieA;
	}
	
	public Text getMovieB() {
		return movieB;
	}
		
	public void write(DataOutput dataOutput) throws IOException {
		movieA.write(dataOutput);
		movieB.write(dataOutput);
	}
	
	public void readFields(DataInput dataInput) throws IOException {
		movieA.readFields(dataInput);
		movieB.readFields(dataInput);
	}
	
	public int compareTo(ComKey obj) {
		int val = movieA.compareTo(obj.movieA);
		if (val == 0) {
			return movieB.compareTo(obj.movieB);
		}
		return val;
	}
	
	public boolean equals(Object obj) {
		if (obj == this) return true;
		if (obj == null || obj.getClass() != getClass()) return false;
		
		ComKey a = (ComKey)obj;
		if (movieA != a.movieA) return false;
		if (movieB != a.movieB) return false;
		
		return true;
	}
	
	public int hashCode() {
		return movieA.hashCode() + movieB.hashCode();
	}
}
