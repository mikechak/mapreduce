import java.io.*;
import java.util.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mrunit.mapreduce.*;
import org.junit.Before;
import org.junit.Test;

public class MovieRecommenderPhase3Test {
	MapDriver<Text,Text,ComKey,Text> mapDriver;
	ReduceDriver<ComKey,Text,Text,Text> reduceDriver;
	
	@Before
	public void setup() {
		MyMapper mapper = new MyMapper();
		MyReducer reducer = new MyReducer();
		
		mapDriver = MapDriver.newMapDriver(mapper);
		reduceDriver = ReduceDriver.newReduceDriver(reducer);
	}
	
	@Test
	public void testMapper() throws IOException {
		mapDriver.withInput(new Text("Movie3,Movie2"), new Text("3,2,2,2,6,9,4"));
		//mapDriver.withOutput(new ComKey("Movie3","Movie2"), new Text("3,2,2,2,6,9,4"));
		mapDriver.withOutput(new ComKey("",""), new Text(""));
		mapDriver.runTest();
	}
}	
