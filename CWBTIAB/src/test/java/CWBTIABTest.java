import java.io.*;
import java.util.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mrunit.mapreduce.*;
import org.junit.Before;
import org.junit.Test;

public class CWBTIABTest {
	MapDriver<Text,Text,Text,MapWritable> mapDriver;
	ReduceDriver<Text,MapWritable,Text,IntWritable> reduceDriver;
	
	@Before
	public void setup() {
		MyMapperPhase2 mapper = new MyMapperPhase2();
		MyReducerPhase2 reducer = new MyReducerPhase2();
		
		mapDriver = MapDriver.newMapDriver(mapper);
		reduceDriver = ReduceDriver.newReduceDriver(reducer);
	}
	
	@Test
	public void testMapperPhase2() throws IOException {
		mapDriver.withInput(new Text("User1"), new Text("egg,banana,bread,egg,yogurt,bread"));
		
		MapWritable map = new MapWritable();
		//map.put(new Text("A"), new IntWritable(1));
		//mapDriver.withOutput(new Text("A"), map);
						
		map.put(new Text("banana"), new IntWritable(1));
		map.put(new Text("bread"), new IntWritable(2));
		map.put(new Text("yogurt"), new IntWritable(1));
		mapDriver.withOutput(new Text("egg"), map);			
				
		map = new MapWritable();
		map.put(new Text("egg"), new IntWritable(2));
		map.put(new Text("bread"), new IntWritable(2));
		map.put(new Text("yogurt"), new IntWritable(1));
		mapDriver.withOutput(new Text("banana"), map);
		
		map = new MapWritable();
		map.put(new Text("egg"), new IntWritable(2));
		map.put(new Text("banana"), new IntWritable(1));
		map.put(new Text("yogurt"), new IntWritable(1));
		mapDriver.withOutput(new Text("bread"), map);
		
		map = new MapWritable();
		map.put(new Text("egg"), new IntWritable(2));
		map.put(new Text("banana"), new IntWritable(1));
		map.put(new Text("bread"), new IntWritable(2));
		mapDriver.withOutput(new Text("yogurt"), map);
		
		mapDriver.runTest();
	}
}
