import org.apache.hadoop.io.*;
import java.io.*;

public class ComKey {
	private Text itemA;
	private Text itemB;
	
	public ComKey() {
		itemA = new Text();
		itemB = new Text();
	}
	
	public ComKey(String a, String b) {
		this();
		itemA.set(a);
		itemB.set(b);
	}
	
	public ComKey(Text a, Text b) {
		this();
		itemA = a;
		itemB = b;
	}
	
	public Text getItemA() {
		return itemA;
	}
	
	public Text getItemB() {
		return itemB;
	}
	
	public void write(DataOutput dataOutput) throws IOException {
		itemA.write(dataOutput);
		itemB.write(dataOutput);
	}
	
	public void readFields(DataInput dataInput) throws IOException {
		itemA.readFields(dataInput);
		itemB.readFields(dataInput);
	}
	
	public int compareTo(ComKey obj) {
		int val = itemA.compareTo(obj.itemA);
		if (val == 0) {
			return itemB.compareTo(obj.itemB);
		}
		return val;
	}
	
	public boolean equals(Object obj) {
		if (obj == this) return true;
		if (obj == null || obj.getClass() != getClass()) return false;
		
		ComKey objA = (ComKey)obj;
		if (objA.itemA != itemA) return false;
		if (objA.itemB != itemB) return false;
		
		return true;
	}
	
	public int hashCode() {
		return itemA.hashCode() + itemB.hashCode();
	}
}
