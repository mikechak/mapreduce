import java.io.*;
import java.util.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.io.*;

public class MyMapperPhase2 extends Mapper<Text,Text,Text,MapWritable> {
	//private Text itemKey = new Text();
	List<String> processed = new ArrayList<String>();
	MapWritable map = new MapWritable();
	
	public void map(Text userId, Text value, Context context) throws IOException, InterruptedException {
		/*
		MapWritable map = new MapWritable();
		map.put(new Text("A"), new IntWritable(0));
		map.put(new Text("A"), new IntWritable(Integer.parseInt(map.get(new Text("A")).toString())+1));
		context.write(new Text("A"), map);
		*/
		
		List<String> items = Arrays.asList(value.toString().split(","));
		
		if (items.size() > 1) {						
			for (String left : items) {
				if (processed.contains(left)) continue;
				
				processed.add(left);
				
				map.clear();
				
				for (String right : items) {
					if (left.equals(right)) continue;
					
					Text itemKey = new Text();
					
					itemKey.set(right);
					
					if (!map.containsKey(itemKey)) {
						map.put(itemKey, new IntWritable(0));
					}
					
					map.put(itemKey, new IntWritable(Integer.parseInt(map.get(itemKey).toString()) + 1));
				}
				
				context.write(new Text(left), map);
				//break;
			}						
		}
	}
}
