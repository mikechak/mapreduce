import java.io.*;
import java.util.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.io.*;

public class MyReducerPhase2 extends Reducer<Text,MapWritable,Text,IntWritable> {
	private Map<ComKey,Integer> map = new HashMap<ComKey,Integer>();
	
	public void reduce(Text item, Iterable<MapWritable> values, Context context) throws IOException, InterruptedException {
		
		map.clear();
		
		for (MapWritable m : values) {
			/*
			for (Map.Entry<Writable, Writable> entry : m.entrySet()) {				
				String name = entry.getKey().toString();
				int count = ((IntWritable)entry.getValue()).get();
				
				ComKey obj = new ComKey(item, (Text)entry.getKey());
				
				if (!map.containsKey(obj)) {
					map.put(obj, 0);
				}
				
				map.put(obj, map.get(obj) + 1);
			}*/
			
			Set<Writable> keys = m.keySet();
			for (Writable key : keys) {
				IntWritable count = (IntWritable)m.get(key);
				
				ComKey obj = new ComKey(item, (Text)key);
				
				if (!map.containsKey(obj)) {
					map.put(obj, 0);
				}
				
				map.put(obj, map.get(obj) + count.get());
			}
		}
		
		for (Map.Entry<ComKey, Integer> m : map.entrySet()) {
			context.write(new Text(m.getKey().getItemA() + " - " + m.getKey().getItemB()), new IntWritable(m.getValue()));
		}
	}
	
	@Override
	public void cleanup(Context context) throws IOException, InterruptedException {
		/*
		for (Map.Entry<ComKey, Integer> m : map.entrySet()) {
			context.write(new Text(m.getKey().getItemA() + " - " + m.getKey().getItemB()), new IntWritable(m.getValue()));
		}*/
	}
}
