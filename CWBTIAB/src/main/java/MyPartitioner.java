import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.io.*;

public class MyPartitioner extends Partitioner<Text,MapWritable> {
	
	public int getPartition(Text key, MapWritable value, int numPartitions) {
		return key.hashCode() % numPartitions;
	}
}
