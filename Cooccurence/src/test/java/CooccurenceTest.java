import java.io.*;
import java.util.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mrunit.mapreduce.*;
import org.junit.Before;
import org.junit.Test;

public class CooccurenceTest {
	MapDriver<LongWritable,Text,ComKey,IntWritable> mapDriver;
	ReduceDriver<ComKey,IntWritable,Text,DoubleWritable> reduceDriver;
	MapReduceDriver<LongWritable,Text,ComKey,IntWritable,Text,DoubleWritable> mapreduceDriver;
	
	@Before
	public void setup() {
		MyMapper mapper = new MyMapper();
		MyReducer reducer = new MyReducer();
		mapDriver = MapDriver.newMapDriver(mapper);
		reduceDriver = ReduceDriver.newReduceDriver(reducer);
		mapreduceDriver = MapReduceDriver.newMapReduceDriver(mapper,reducer);
	}
	
	@Test
	public void testMapper() throws IOException {
		Configuration conf = mapDriver.getConfiguration();
		conf.set("delimiter", " ");
		conf.setInt("size", 2);
		
		mapDriver.withInput(new LongWritable(), new Text("w1 w2 w3"));
		
		mapDriver.withOutput(new ComKey("w1","w2"), new IntWritable(1));
		mapDriver.withOutput(new ComKey("w1","w3"), new IntWritable(1));
		mapDriver.withOutput(new ComKey("w2","w1"), new IntWritable(1));
		mapDriver.withOutput(new ComKey("w2","w3"), new IntWritable(1));
		mapDriver.withOutput(new ComKey("w3","w1"), new IntWritable(1));
		mapDriver.withOutput(new ComKey("w3","w2"), new IntWritable(1));		
		mapDriver.withOutput(new ComKey("w1","*"), new IntWritable(2));
		mapDriver.withOutput(new ComKey("w2","*"), new IntWritable(2));
		mapDriver.withOutput(new ComKey("w3","*"), new IntWritable(2));		
		//mapDriver.withOutput(new ComKey("w1","w2"), new IntWritable(1));
		mapDriver.runTest();
	}
	
	@Test
	public void testReducer() throws IOException {
		List<IntWritable> values = new ArrayList<IntWritable>();		
		values.add(new IntWritable(2));
		reduceDriver.withInput(new ComKey("w1","*"), values);
		
		values = new ArrayList<IntWritable>();
		values.add(new IntWritable(1));
		reduceDriver.withInput(new ComKey("w1","w2"), values);
		
		//reduceDriver.withOutput(new Text("w1 - *"), new DoubleWritable(0.5));
		//reduceDriver.withOutput(new Text("w1 - w2"), new DoubleWritable(0.5));
		reduceDriver.withOutput(new Text("w1 - w2"), new DoubleWritable(0.5));
		
		reduceDriver.runTest();
	}
	
	@Test
	public void testMapReducer() throws IOException {
		mapreduceDriver.withInput(new LongWritable(), new Text("java is a great language"));
		mapreduceDriver.withInput(new LongWritable(), new Text("java is a programming language"));
		mapreduceDriver.withInput(new LongWritable(), new Text("java is a green fun language"));
		mapreduceDriver.withInput(new LongWritable(), new Text("java is great"));
		mapreduceDriver.withInput(new LongWritable(), new Text("programming with java is fun"));
		
		
	}
}
