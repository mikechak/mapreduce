import java.io.*;
import java.util.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.io.*;

public class MyReducer extends Reducer<ComKey,IntWritable,Text,DoubleWritable> {
	private long total = 0;
	//private Map<String,Integer> map = new HashMap<String,Integer>();
	private Map<String,Long> map = new HashMap<String,Long>();
	private String word;
	
	public void setup(Context context) {
		
	}
	
	public void reduce(ComKey key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
		//context.write(new Text(key.getLeft().toString() + " - " + key.getRight().toString()), new DoubleWritable(0.5));
		
		String tmp = key.getRight().toString();
		
		/*
		if (key.getRight().toString().equals("*")) {
			word = key.getLeft().toString();
			for (IntWritable val : values) {
				total += val.get();
			}
		}
		else {
			if (!map.containsKey(key.getRight().toString())) map.put(key.getRight().toString(), 0);
			map.put(key.getRight().toString(), map.get(key.getRight().toString()) + 1);			
		}*/
		
		if (key.getRight().toString().equals("*")) {
			word = key.getLeft().toString();
			total = 0;
			for (IntWritable val : values) {
				total += val.get();
			}
			if (!map.containsKey(word)) map.put(word, 0L);
			map.put(word, map.get(word) + total);
		}
		else {
			word = key.getLeft().toString();
			total = 0;
			for (IntWritable val : values) {
				total += val.get();
			}
			if (map.containsKey(word)) {
				context.write(new Text(key.getLeft() + " - " + key.getRight()), new DoubleWritable(total / map.get(word).doubleValue()));
			}
		}
	}

	public void cleanup(Context context) throws IOException, InterruptedException {
		/*
		for (Map.Entry<String, Integer> entry : map.entrySet()) {
			context.write(new Text(word + " - " + entry.getKey()), new DoubleWritable(entry.getValue()/(double)total));
		}*/
	}
}
