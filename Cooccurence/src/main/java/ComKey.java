import org.apache.hadoop.io.*;
import java.io.*;

public class ComKey implements Writable, WritableComparable<ComKey> {
	private Text left;
	private Text right;
	
	public ComKey() {
		left = new Text();
		right = new Text();
	}
	
	public ComKey(String left, String right) {
		this();
		this.left.set(left);
		this.right.set(right);
	}
	
	public void set(String left, String right) {
		this.left.set(left);
		this.right.set(right);
	}
	
	public void setLeft(String left) {
		this.left.set(left);
	}
	
	public void setRight(String right) {
		this.right.set(right);
	}
	
	public Text getLeft() {
		return left;
	}
	
	public Text getRight() {
		return right;
	}
	
	public void write(DataOutput dataOutput) throws IOException {
		left.write(dataOutput);
		right.write(dataOutput);
	}
	
	public void readFields(DataInput dataInput) throws IOException {
		left.readFields(dataInput);
		right.readFields(dataInput);
	}
	
	public int compareTo(ComKey obj) {
		int val = this.left.compareTo(obj.left);
		if (val == 0) {
			return this.right.compareTo(obj.right);
		}
		return val;
	}
	
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null || obj.getClass() != getClass()) return false;
		
		ComKey a = (ComKey)obj;
		if (!left.equals(a.left)) return false;
		if (!right.equals(a.right)) return false;
		
		return true;
	}
	
	public int hashCode() {
		return left.hashCode() + right.hashCode();
	}
}
