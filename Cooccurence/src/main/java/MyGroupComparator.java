import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;

public class MyGroupComparator extends WritableComparator {
	public MyGroupComparator() {
		super(ComKey.class, true);
	}
	
	@Override
	public int compare(WritableComparable a, WritableComparable b) {
		ComKey objA = (ComKey)a;
		ComKey objB = (ComKey)b;
		
		//return objA.getLeft().compareTo(objB.getLeft());
		int val = objA.getLeft().compareTo(objB.getLeft());
		if (val == 0) {
			return objA.getRight().compareTo(objB.getRight());
		}
		return val;
	}
}
