import java.io.*;
import java.util.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.io.*;
//import com.google.common.base.Splitter;
//import com.google.common.collect.Lists;

public class MyMapper extends Mapper<LongWritable,Text,ComKey,IntWritable> {
	private int size;
	private int sum;
	private ComKey cKey = new ComKey();
	private String delimiter;
	//private Splitter splitter;
	private Map<String,Integer> map = new HashMap<String,Integer>();
	
	public void setup(Context context) {
		size = context.getConfiguration().getInt("size", 0);
		delimiter = context.getConfiguration().get("delimiter");
		//splitter = Splitter.on(delimiter);
	}
	
	public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
		//List<String> lst = Lists.newArrayList(splitter.split(value.toString()));
		List<String> lst = new ArrayList<String>(Arrays.asList(value.toString().split(delimiter)));
		//context.write(new ComKey(lst.get(0), lst.get(1)), new IntWritable(1));
		
		for (int i=0;i<lst.size();i++) {
			for (int j=i-size;j<=i+size;j++) {
				if (j >= 0 && j < lst.size() && j != i) {
					if (!map.containsKey(lst.get(i))) map.put(lst.get(i), 0);
					map.put(lst.get(i), map.get(lst.get(i))+1);
					cKey.set(lst.get(i), lst.get(j));
					context.write(cKey, new IntWritable(1));
					//context.write(new ComKey(lst.get(i), lst.get(j)), new IntWritable(1));
				}
			}
		}
	}
	
	public void cleanup(Context context) throws IOException, InterruptedException {		
		for (Map.Entry<String,Integer> entry : map.entrySet()) {
			cKey.set(entry.getKey(), "*");
			context.write(cKey, new IntWritable(entry.getValue()));
		}
	}
}
