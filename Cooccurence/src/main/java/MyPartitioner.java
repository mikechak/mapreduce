import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.io.*;

public class MyPartitioner extends Partitioner<ComKey,Text> {
	public int getPartition(ComKey key, Text value, int numPartitions) {
		return key.getLeft().hashCode() % numPartitions;
	}
}
