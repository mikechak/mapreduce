import java.io.IOException;
import java.util.*;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mrunit.mapreduce.*;
import org.junit.Before;
import org.junit.Test;

public class ReducerJoin2Test {
	MapDriver<LongWritable,Text,ComKey,Text> mapDriver;
	ReduceDriver<ComKey,Text,NullWritable,Text> reduceDriver;
	MapReduceDriver<LongWritable,Text,ComKey,Text,NullWritable,Text> mapreduceDriver;
	MultipleInputsMapReduceDriver<LongWritable,Text,ComKey,Text> multipleInputsMapReduceDriver;
	
	@Before
	public void setup() {
		MyMapper mapper = new MyMapper();
		MyReducer reducer = new MyReducer();
		
		mapDriver = MapDriver.newMapDriver(mapper);
		reduceDriver = ReduceDriver.newReduceDriver(reducer);
		mapreduceDriver = MapReduceDriver.newMapReduceDriver(mapper,reducer);
		multipleInputsMapReduceDriver = new MultipleInputsMapReduceDriver();
	}
	
	@Test
	public void testMapperFile1() throws IOException {
		mapDriver.getConfiguration().set("separator", ",");
		mapDriver.getConfiguration().set("userdata.txt", "1");
		mapDriver.getConfiguration().set("ccdata.txt", "2");
		
		Path mapInputPath = new Path("userdata.txt");
		mapDriver.setMapInputPath(mapInputPath);
		mapDriver.withInput(new LongWritable(), new Text("cdd8dde3-0349-4f0d-b97a-7ae84b687f9c,Esther,Garner,4071 Haven Lane,Okemos,MI"));		
		mapDriver.withOutput(new ComKey("cdd8dde3-0349-4f0d-b97a-7ae84b687f9c","1"), new Text("Esther,Garner,4071 Haven Lane,Okemos,MI"));		
		mapDriver.runTest();
	}
	
	@Test
	public void testMapperFile2() throws IOException {
		mapDriver.getConfiguration().set("separator", ",");
		mapDriver.getConfiguration().set("userdata.txt", "1");
		mapDriver.getConfiguration().set("ccdata.txt", "2");
		
		Path mapInputPath = new Path("ccdata.txt");
		mapDriver.setMapInputPath(mapInputPath);
		mapDriver.withInput(new LongWritable(), new Text("cdd8dde3-0349-4f0d-b97a-7ae84b687f9c,517-706-9565,EstherJGarner@teleworm.us,Waskepter38,noL2ieghie,MasterCard,5305687295670850"));
		mapDriver.withOutput(new ComKey("cdd8dde3-0349-4f0d-b97a-7ae84b687f9c","2"), new Text("517-706-9565,EstherJGarner@teleworm.us,Waskepter38,noL2ieghie,MasterCard,5305687295670850"));
		mapDriver.runTest();
	}
	
	@Test
	public void testReduce() throws IOException {
		List<Text> values = new ArrayList<Text>();
		values.add(new Text("Esther,Garner,4071 Haven Lane,Okemos,MI"));
		values.add(new Text("517-706-9565,EstherJGarner@teleworm.us,Waskepter38,noL2ieghie,MasterCard,5305687295670850"));
		
		reduceDriver.withInput(new ComKey("cdd8dde3-0349-4f0d-b97a-7ae84b687f9c","1"),values);
		//reduceDriver.withInput(new ComKey("cdd8dde3-0349-4f0d-b97a-7ae84b687f9c","2"),"");
		reduceDriver.withOutput(NullWritable.get(), new Text("cdd8dde3-0349-4f0d-b97a-7ae84b687f9c,Esther,Garner,4071 Haven Lane,Okemos,MI,517-706-9565,EstherJGarner@teleworm.us,Waskepter38,noL2ieghie,MasterCard,5305687295670850"));
		reduceDriver.runTest();
	}
	
	@Test
	public void testMapReduce() throws IOException {
		//multipleInputsMapReduceDriver.addMapper(mapDriver);
		//MyMapper m = new MyMapper();
		//multipleInputsMapReduceDriver.addInput(m, new LongWritable(), new Text("cdd8dde3-0349-4f0d-b97a-7ae84b687f9c,Esther,Garner,4071 Haven Lane,Okemos,MI"));
	}
}
