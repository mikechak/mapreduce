import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.io.*;

public class CustomPartitioner extends Partitioner<ComKey, Text> {
	@Override
	public int getPartition(ComKey key, Text value, int numPartitions) {
		return key.getGuid().hashCode() % numPartitions;
	}
}
