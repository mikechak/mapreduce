import java.io.*;
import java.util.*;

import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
//import com.google.common.base.*;
//import com.google.common.collect.Lists;

public class MyMapper extends Mapper<LongWritable,Text,ComKey,Text> 
{
	private int joinOrder;
	String separator;
	//private Splitter splitter;
	//private Joiner joiner;
	private ComKey comKey = new ComKey();
	
	@Override
	protected void setup(Context context) throws IOException, InterruptedException 
	{
		separator = context.getConfiguration().get("separator");
		FileSplit fileSplit = (FileSplit)context.getInputSplit();
		joinOrder = Integer.parseInt(context.getConfiguration().get(fileSplit.getPath().getName()));
		
		//splitter = Splitter.on(separator).trimResults();
		//joiner = Joiner.on(separator);
	}
	
	@Override
	protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException 
	{
		//List<String> values = Lists.newArrayList(splitter.split(value.toString()));
		//List<String> values = new Arrays.asList(value.toString().split(separator));
		List<String> values = new ArrayList<String>(Arrays.asList(value.toString().split(separator)));
		//String joinKey = values.remove(0);
		
		//String joinKey = values.get(0);
		
		//int len = values.size();
		
		String joinKey = values.remove(0);
		
		//String val = joiner.join(values);
		String val = StringUtils.join(values,separator);
		
		comKey.Set(joinKey, Integer.toString(joinOrder));
		
		context.write(comKey, new Text(val));
	}
}


