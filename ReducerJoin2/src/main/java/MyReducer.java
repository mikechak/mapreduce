import java.util.*;
import java.io.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;

public class MyReducer extends Reducer<ComKey,Text,NullWritable,Text> 
{
	private StringBuilder sb = new StringBuilder();
	
	public void reduce(ComKey key, Iterable<Text> values, Context context) throws IOException, InterruptedException 
	{
		sb.append(key.getGuid()).append(",");
		
		for (Text val : values) {
			sb.append(val.toString()).append(",");
		}
		
		sb.setLength(sb.length()-1);
		
		context.write(NullWritable.get(), new Text(sb.toString()));
	}
}
