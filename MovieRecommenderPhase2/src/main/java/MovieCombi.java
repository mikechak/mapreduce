
public class MovieCombi {
	private Movie movieA;
	private Movie movieB;
	
	public MovieCombi(Movie movieA, Movie movieB) {
		this.movieA = movieA;
		this.movieB = movieB;
	}
	
	public Movie getMovieA() {
		return movieA;
	}
	
	public Movie getMovieB() {
		return movieB;
	}
}
