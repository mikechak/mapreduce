import java.io.*;
import java.util.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;

public class MyReducer extends Reducer<Text,Movie,Text,Text> {
	private Text p2Key = new Text();
	private Text p2Value = new Text();

	public void reduce(Text key, Iterable<Movie> values, Context context) throws IOException, InterruptedException {
		List<Movie> movies = new ArrayList<Movie>();
		
		for (Movie val : values) {
			movies.add(new Movie(val));
		}
		
		List<MovieCombi> combi = generateCombinations(movies);
		
		for (MovieCombi obj : combi) {
			p2Key.set(obj.getMovieA().getMovie().toString() + "," + obj.getMovieB().getMovie().toString());
			
			Integer product = obj.getMovieA().getRating().get() * obj.getMovieB().getRating().get();
			
			Integer rating1Squared = obj.getMovieA().getRating().get() * obj.getMovieA().getRating().get();
			
			Integer rating2Squared = obj.getMovieB().getRating().get() * obj.getMovieB().getRating().get();
			
			p2Value.set(obj.getMovieA().getRating().toString() + "," 
					+ obj.getMovieA().getNumOfRaters().toString() + "," 
					+ obj.getMovieB().getRating().toString() + "," 
					+ obj.getMovieB().getNumOfRaters().toString() + "," 
					+ product.toString() + "," 
					+ rating1Squared.toString() + "," 
					+ rating2Squared.toString());
			
			context.write(p2Key, p2Value);
		}
	}
	
	public List<MovieCombi> generateCombinations(List<Movie> list) {
		List<MovieCombi> combi = new ArrayList<MovieCombi>();
		
		for (int i=0; i<list.size(); i++) {
			for (int j=i+1; j<list.size(); j++) {
				combi.add(new MovieCombi(list.get(i),list.get(j)));
			}
		}
		return combi;
	}
}
