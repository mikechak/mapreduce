import java.io.*;
import java.util.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;

public class MyMapper extends Mapper<LongWritable,Text,Text,Movie> {
	private Text user = new Text();
	
	public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
		String[] lst = value.toString().split(",");
		
		user.set(lst[0]);
		
		context.write(user, new Movie(lst[1], Integer.parseInt(lst[2]), Integer.parseInt(lst[3])));
	}
	
	/*
	public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
		String[] lst = value.toString().split(",");
		
		List<ComKey> combi = generateCombinations(lst);
		
		for (ComKey obj : combi) {
			
		}
		
	}

	private List<ComKey> generateCombinations(String[] lst) {
		List<ComKey> combi = new ArrayList<ComKey>();
		for (int i=0; i<lst.length; i++) {
			for (int j=i+1; j<lst.length; j++) {
				combi.add(new ComKey(lst[i],lst[j],i,j));
			}
		}
		return combi;				
	}*/
}
