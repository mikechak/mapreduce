import org.apache.hadoop.io.*;
import java.io.*;

public class Movie implements Writable {
	private Text movie;
	private IntWritable rating;
	private IntWritable numOfRaters;
	
	public Movie() {
		movie = new Text();
		rating = new IntWritable();
		numOfRaters = new IntWritable();
	}
	
	public Movie(Movie obj) {
		this();
		this.movie.set(obj.movie);
		this.rating.set(obj.rating.get());
		this.numOfRaters.set(obj.numOfRaters.get());
	}
	
	public Movie(String movie, int rating, int numOfRaters) {
		this();
		this.movie.set(movie);
		this.rating.set(rating);
		this.numOfRaters.set(numOfRaters);
	}
	
	public void write(DataOutput dataOutput) throws IOException {
		movie.write(dataOutput);
		rating.write(dataOutput);
		numOfRaters.write(dataOutput);
	}
	
	public void readFields(DataInput dataInput) throws IOException {
		movie.readFields(dataInput);
		rating.readFields(dataInput);
		numOfRaters.readFields(dataInput);
	}
	
	public Text getMovie() {
		return movie;
	}
	
	public IntWritable getRating() {
		return rating;
	}
	
	public IntWritable getNumOfRaters() {
		return numOfRaters;
	}
}
