import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;

public class MyGroupComparator extends WritableComparator {
	public MyGroupComparator() {
		super(ComKey.class, true);
	}
	
	@Override
	public int compare(WritableComparable objA, WritableComparable objB) {
		ComKey a = (ComKey)objA;
		ComKey b = (ComKey)objB;
		
		return a.getMovieA().compareTo(b.getMovieA());
	}
}
