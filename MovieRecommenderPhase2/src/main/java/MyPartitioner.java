import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;

public class MyPartitioner extends Partitioner<ComKey,Text> {
	
	public int getPartition(ComKey key, Text value, int numPartitions) {
		return key.getMovieA().hashCode() % numPartitions;
	}

}
