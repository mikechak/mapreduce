import java.io.*;
import java.util.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mrunit.mapreduce.*;
import org.junit.Before;
import org.junit.Test;

public class MovieRecommenderPhase2Test {
	MapDriver<LongWritable,Text,Text,Movie> mapDriver;
	ReduceDriver<Text,Movie,Text,Text> reduceDriver;
	
	@Before
	public void setup() {
		MyMapper mapper = new MyMapper();
		MyReducer reducer = new MyReducer();
		
		mapDriver = MapDriver.newMapDriver(mapper);
		reduceDriver = ReduceDriver.newReduceDriver(reducer);
	}
	
	@Test
	public void testMapper() throws IOException {
		mapDriver.withInput(new LongWritable(), new Text("User1,Movie1,1,2"));
		mapDriver.withOutput(new Text("User1"), new Movie("Movie1",1,2));
		mapDriver.runTest();
	}
	
	@Test
	public void testReducer() throws IOException {
		
	}
}
