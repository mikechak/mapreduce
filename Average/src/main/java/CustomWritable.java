import org.apache.hadoop.io.*;
import java.io.*;

public class CustomWritable implements Writable 
{
	private IntWritable sum;
	private IntWritable count;
	
	public CustomWritable() {
		sum = new IntWritable(0);
		count = new IntWritable(0);
	}
	
	public CustomWritable(int sum, int count) {
		this.sum = new IntWritable(sum);
		this.count = new IntWritable(count);
	}
	
	public void set(int sum, int count) {
		this.sum.set(sum);
		this.count.set(count);
	}
		
	public void write(DataOutput dataOutput) throws IOException {
		this.sum.write(dataOutput);
		this.count.write(dataOutput);
	}
		
	public void readFields(DataInput dataInput) throws IOException {
		this.sum.readFields(dataInput);
		this.count.readFields(dataInput);
	}
	
	@Override
	public String toString() {
		return sum.toString() + ": " + count.toString();
	}
	
	public IntWritable getSum() {
		return sum;
	}
	
	public IntWritable getCount() {
		return count;
	}
	
	public int compareTo(CustomWritable o) {
		int compareVal = this.sum.compareTo(o.getSum());
		if (compareVal != 0) {
			return compareVal;
		}
		return this.count.compareTo(o.getCount());		
	}
	
	@Override
	public int hashCode() {
		int result = sum.hashCode();
		result = 163 * result + count.hashCode();
		return result;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		
		CustomWritable that = (CustomWritable)o;
		
		if (!sum.equals(that.sum)) return false;
		if (!count.equals(that.count)) return false;
		
		return true;
	}
}
