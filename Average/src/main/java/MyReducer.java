import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class MyReducer extends Reducer<Text, CustomWritable, Text, IntWritable>
	{
		int sum = 0;
		int count = 0;
		IntWritable avg = new IntWritable();
		
		public void reduce(Text key, Iterable<CustomWritable> values, Context context) throws IOException, InterruptedException
		{
			for (CustomWritable val : values) {
				sum += val.getSum().get();
				count += val.getCount().get();
			}
			avg.set(sum/count);
			context.write(key, avg);
		}
	}
