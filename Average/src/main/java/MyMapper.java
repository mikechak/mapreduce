import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class MyMapper extends Mapper<LongWritable, Text, Text, CustomWritable>
	{
		private Map<String, CustomWritable> pairMap = new HashMap<String, CustomWritable>();
				
		@Override
		public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException
		{						
			StringTokenizer itr = new StringTokenizer(value.toString());
			while (itr.hasMoreTokens())
			{
				String token = itr.nextToken();
				
				CustomWritable pair = pairMap.get(token);
				if (pair == null) {
					pair = new CustomWritable();
					pairMap.put(token, pair);
				}
				int sum = pair.getSum().get() + 1;
				int count = pair.getCount().get() + 1;
				pair.set(sum, count);				
			}
		}
		
		@Override
		public void cleanup(Context context) throws IOException, InterruptedException 
		{
			Set<String> keys = pairMap.keySet();
			Text keyText = new Text();
			for (String key:keys)
			{
				keyText.set(key);
				context.write(keyText, pairMap.get(key));
			}
		}
	}
