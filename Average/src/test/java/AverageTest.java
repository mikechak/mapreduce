import java.util.*;
import java.io.*;
import org.apache.hadoop.mrunit.mapreduce.*;
//import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.io.*;
import org.junit.Before;
import org.junit.Test;

public class AverageTest {
	MapDriver<LongWritable, Text, Text, CustomWritable> mapDriver;
	ReduceDriver<Text, CustomWritable, Text, IntWritable> reduceDriver;
	MapReduceDriver<LongWritable, Text, Text, CustomWritable, Text, IntWritable> mapreduceDriver;
	
	@Before
	public void setup()
	{
		MyMapper mapper = new MyMapper();
		MyReducer reducer = new MyReducer();
		mapDriver = MapDriver.newMapDriver(mapper);
		reduceDriver = ReduceDriver.newReduceDriver(reducer);
		mapreduceDriver = MapReduceDriver.newMapReduceDriver(mapper, reducer);
	}
		
	@Test
	public void testMapper() throws IOException 
	{
		mapDriver.withInput(new LongWritable(), new Text("Man Utd Man"));
		//List<CustomWritable> values = new ArrayList<CustomWritable>();
		//values.add(new CustomWritable(2,2));
		//List<CustomWritable> values2 = new ArrayList<CustomWritable>();
		//values2.add(new CustomWritable(1,1));
		mapDriver.withOutput(new Text("Utd"), new CustomWritable(1,1));
		mapDriver.withOutput(new Text("Man"), new CustomWritable(2,2));
		
		mapDriver.runTest();
	}
	
	@Test
	public void testReducer() throws IOException
	{
		List<CustomWritable> values = new ArrayList<CustomWritable>();
		values.add(new CustomWritable(1,1));
		values.add(new CustomWritable(1,1));
		reduceDriver.withInput(new Text("Man"), values);
		reduceDriver.withOutput(new Text("Man"), new IntWritable(1));
		reduceDriver.runTest();
	}
	
	@Test
	public void testMapReduce() throws IOException
	{
		mapreduceDriver.withInput(new LongWritable(), new Text("Man Utd ZZ"));
				
		mapreduceDriver.withOutput(new Text("Man"), new IntWritable(1));
		mapreduceDriver.withOutput(new Text("Utd"), new IntWritable(1));
		mapreduceDriver.withOutput(new Text("ZZ"), new IntWritable(1));
		mapreduceDriver.runTest();
	}
}
