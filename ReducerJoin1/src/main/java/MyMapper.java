import java.io.IOException;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Mapper.Context;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;


public class MyMapper extends Mapper<LongWritable,Text,ComKey,Text> {
		public void map(LongWritable key,Text value, Context context) throws IOException, InterruptedException {
			Text delimiter = new Text();
			delimiter.set(context.getConfiguration().get("input.delimiter"));
			//context.write(new ComKey(), delimiter);
			
			//context.getCounter("foo","bar").increment(1);
			//context.getCounter("foo","bar").increment(1);
			context.getCounter(ReducerJoin1.MyCounters.MAN).increment(1);
			
			Path mapInputPath = null;
			if (context.getInputSplit() instanceof FileSplit) {
				mapInputPath = ((FileSplit) context.getInputSplit()).getPath();
				//context.write(new ComKey(), new Text(mapInputPath.getName()));
			}
			//else {
			//	context.write(new ComKey(), new Text("B"));
			//}
			Text tmp = new Text();
			if (mapInputPath != null) {
				tmp.set(mapInputPath.getName());
			}			
			context.write(new ComKey(), tmp);
			
			
			
		}
	}
