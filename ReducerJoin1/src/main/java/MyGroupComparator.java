import org.apache.hadoop.io.*;

public class MyGroupComparator extends WritableComparator {
	public MyGroupComparator() {
		super(ComKey.class, true);
	}
	
	public int compare(WritableComparable a, WritableComparable b) {
		ComKey objA = (ComKey)a;
		ComKey objB = (ComKey)b;
		
		return objA.getGuid().compareTo(objB.getGuid());
	}
}
