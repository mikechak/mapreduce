import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Reducer.Context;

public class MyReducer extends Reducer<ComKey,Text,NullWritable,Text> {
		public void reduce(ComKey key,Iterable<Text> value, Context context) throws IOException, InterruptedException {
			//Path[] uri =  DistributedCache.getLocalCacheFiles(context.getConfiguration());
			URI[] uri = context.getCacheFiles();			
			//context.write(NullWritable.get(), new Text(uri[0].toString()));
			//context.write(NullWritable.get(), new Text(""));
			//File f = new File(uri[0]);
			
			Path path = new Path(uri[0].toString());
			FileSystem fs = path.getFileSystem(context.getConfiguration());
			BufferedReader br = new BufferedReader(new InputStreamReader(fs.open(path)));
			String line;
			line = br.readLine();
			/*
			while (line != null) {
				System.out.println(line);
				line = br.readLine();
			}*/
			context.write(NullWritable.get(), new Text(line));
			
			/*
			BufferedReader fis = new BufferedReader(new FileReader(uri[0].toString()));		
			String str = null;
            while ((str = fis.readLine())!= null)
            {
            	break;
            }			
            context.write(NullWritable.get(), new Text(str));
            */
		}
	}
