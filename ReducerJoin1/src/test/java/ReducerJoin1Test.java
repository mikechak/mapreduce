import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.Mapper.Context;
import org.apache.hadoop.mapreduce.filecache.*;

import static org.junit.Assert.*;
import java.util.*;
import java.io.File;
import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mrunit.mapreduce.*;
//import org.apache.hadoop.mrunit.mock;
import org.junit.Before;
import org.junit.Test;

import junit.framework.Assert;
//import org.mockito.Mock;

public class ReducerJoin1Test {
	MapDriver<LongWritable,Text,ComKey,Text> mapDriver;
	ReduceDriver<ComKey,Text,NullWritable,Text> reduceDriver;
	MapReduceDriver<LongWritable,Text,ComKey,Text,NullWritable,Text> mapreduceDriver;
	
	@Before
	public void setup() {
		MyMapper mapper = new MyMapper();
		MyReducer reducer = new MyReducer();
		
		mapDriver = MapDriver.newMapDriver(mapper);
		reduceDriver = ReduceDriver.newReduceDriver(reducer);
		mapreduceDriver = MapReduceDriver.newMapReduceDriver(mapper,reducer);
	}
	
	@Test
	public void mapperTest() throws IOException {						
		
		mapDriver.getConfiguration().set("input.delimiter", "x");

		Path mapInputPath = new Path("myfile");
		mapDriver.setMapInputPath(mapInputPath);
		
		mapDriver.withInput(new LongWritable(0), new Text(""));
		//mapDriver.withOutput(new ComKey(), new Text("x"));
		mapDriver.withOutput(new ComKey(), new Text("myfile"));
		mapDriver.runTest();
		//context = mock(Mapper.Context.class);
		
		
	}
	
	@Test
	public void counterTest() throws IOException {
		mapDriver.getConfiguration().set("input.delimiter", "x");
		Path mapInputPath = new Path("myfile");
		mapDriver.setMapInputPath(mapInputPath);
		
		mapDriver.withInput(new LongWritable(0), new Text(""));
		mapDriver.withOutput(new ComKey(), new Text("myfile"));
		mapDriver.run();
		
		Counters counters = mapDriver.getCounters();
		//Counters counters = mapDriver.getContext().getCo

		//assertEquals(2, counters.findCounter("foo", "bar").getValue());
		Assert.assertEquals(1, counters.findCounter(ReducerJoin1.MyCounters.MAN).getValue());
		//Assert.assertEquals(1, mapDriver.getContext().getCounter(ReducerJoin1.MyCounters.MAN).getValue());
	}
	
	@Test
	public void cacheTest() throws IOException {
		//mapDriver.getConfiguration().set("input.delimiter", "x");
		//Path mapInputPath = new Path("myfile");
		//mapDriver.setMapInputPath(mapInputPath);
		
		Configuration conf = reduceDriver.getConfiguration();
		//Context context = mapDriver.getContext();
		
		//DistributedCache c = new DistributedCache();

		List<Text> values = new ArrayList<Text>();
		values.add(new Text(""));
		
		//Job job = Job.getInstance(conf);
		
		DistributedCache.addCacheFile(new File("input/README.txt").toURI(), conf);
		
		reduceDriver
		.withInput(new ComKey(), values)
		.withOutput(NullWritable.get(), new Text("ManUtd"))
		.runTest();
	}
}
