//https://pragmaticintegrator.wordpress.com/2013/08/26/unit-testing-a-java-hadoop-job/

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.junit.Before;
import org.junit.Test;
import java.io.IOException;

public class MapperTest {
 
    MapDriver<Text, Text, Text, Text> mapDriver;
 
    @Before
    public void setUp() {
        WordMapper mapper = new WordMapper();
        mapDriver = MapDriver.newMapDriver(mapper);
    }
 
    @Test
    public void testMapper() throws IOException {
        mapDriver.withInput(new Text("a"), new Text("ein"));
        mapDriver.withInput(new Text("a"), new Text("zwei"));
        mapDriver.withInput(new Text("c"), new Text("drei"));
        mapDriver.withOutput(new Text("a"), new Text("ein"));
        mapDriver.withOutput(new Text("a"), new Text("zwei"));
        mapDriver.withOutput(new Text("c"), new Text("drei"));
        mapDriver.runTest();
    }
}
