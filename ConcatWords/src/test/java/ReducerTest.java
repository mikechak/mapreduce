//https://pragmaticintegrator.wordpress.com/2013/08/26/unit-testing-a-java-hadoop-job/
	
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
 
public class ReducerTest {
 
    ReduceDriver<Text, Text, Text, Text> reduceDriver;
 
    @Before
    public void setUp() {
        WordReducer reducer = new WordReducer();
        reduceDriver = ReduceDriver.newReduceDriver(reducer);
    }
 
    @Test
    public void testReducer() throws IOException {
        List<Text> values = new ArrayList<Text>();
        values.add(new Text("ein"));
        values.add(new Text("zwei"));
        reduceDriver.withInput(new Text("a"), values);
        reduceDriver.withOutput(new Text("a"), new Text("|ein|zwei"));
        reduceDriver.runTest();
    }
}
