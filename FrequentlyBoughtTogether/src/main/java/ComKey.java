import org.apache.hadoop.io.*;
import java.io.*;

public class ComKey implements Writable, WritableComparable<ComKey> {
	private Text productA;
	private Text productB;
	
	public ComKey() {
		productA = new Text();
		productB = new Text();
	}
	
	public ComKey(String a, String b) {
		this();
		productA.set(a);
		productB.set(b);
	}
	
	public Text getProductA() {
		return productA;
	}
	
	public Text getProductB() {
		return productB;
	}
	
	public void write(DataOutput out) throws IOException {
		this.productA.write(out);
		this.productB.write(out);
	}
	
	public void readFields(DataInput in) throws IOException {
		this.productA.readFields(in);
		this.productB.readFields(in);
	}
	
	public int compareTo(ComKey obj) {
		int val = this.productA.compareTo(obj.productA);
		if (val == 0) {
			return this.productB.compareTo(obj.productB);
		}
		return val;
	}
	
	public boolean equals(Object obj) {
		if (obj == this) return true;
		if (obj == null || obj.getClass() != getClass()) return false;
		
		ComKey o = (ComKey)obj;
		if (!this.productA.equals(o.productA)) return false;
		if (!this.productB.equals(o.productB)) return false;
		
		return true;
	}
	
	public int hashCode() {
		return this.productA.hashCode() + this.productB.hashCode();
	}
}
