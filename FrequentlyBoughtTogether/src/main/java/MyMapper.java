import java.io.*;
import java.util.*;
import org.apache.hadoop.mapreduce.*;

import com.google.common.collect.Lists;

import org.apache.hadoop.io.*;

public class MyMapper extends Mapper<LongWritable,Text,ComKey,IntWritable> {

	public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
		
		List<String> values = Arrays.asList(value.toString().split(","));
		
		TreeSet<String> sorted = new TreeSet<String>();
		
		for (String val : values) {
			sorted.add(val);
		}
		
		values = new ArrayList<String>();
		
		for (String val : sorted) {
			values.add(val);
		}
		
		List<ComKey> combi = generateCombinations(values);
		
		for (ComKey o : combi) {
			context.write(o, new IntWritable(1));
		}				
	}
	
	public List<ComKey> generateCombinations(List<String> list) {
		List<ComKey> combi = new ArrayList<ComKey>();
		for (int i=0; i<list.size(); i++) {
			for (int j=i+1; j<list.size(); j++) {
				combi.add(new ComKey(list.get(i),list.get(j)));
			}
		}
		return combi;
	}
	/*
	public Map<String,String> generateCombinations(List<String> list) {
		Map<String,String> combi = new HashMap<String,String>();
		
		for (int i=0; i<list.size(); i++) {
			for (int j=i+1; j<list.size(); j++) {
				combi.put(list.get(i), list.get(j));
			}
		}
		
		return combi;
	}*/
}
