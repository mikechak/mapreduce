import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.io.*;

public class MyGroupComparator extends WritableComparator {
	public MyGroupComparator() {
		super(ComKey.class, true);
	}
	
	@Override
	public int compare(WritableComparable a, WritableComparable b) {
		ComKey objA = (ComKey)a;
		ComKey objB = (ComKey)b;
		
		//return objA.getProductA().compareTo(objB.getProductA());
		int val = objA.getProductA().compareTo(objB.getProductA());
		if (val == 0) {
			return objA.getProductB().compareTo(objB.getProductB());
		}
		return val;
	}
}
