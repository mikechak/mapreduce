import java.io.*;
import java.util.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.io.*;

public class MyReducer extends Reducer<ComKey,IntWritable,Text,IntWritable> {

	public void reduce(ComKey key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
		int sum = 0;
		for (IntWritable val : values) {
			sum += val.get();
		}
		context.write(new Text(key.getProductA().toString() + " - " + key.getProductB().toString()), new IntWritable(sum));
	}
}
