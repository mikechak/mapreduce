import java.io.*;
import java.util.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mrunit.mapreduce.*;
import org.junit.Before;
import org.junit.Test;

public class FrequentlyBoughtTogetherTest {
	MapDriver<LongWritable,Text,ComKey,IntWritable> mapDriver;
	ReduceDriver<ComKey,IntWritable,Text,IntWritable> reduceDriver;
	
	@Before
	public void setup() {
		MyMapper mapper = new MyMapper();
		MyReducer reducer = new MyReducer();
		
		mapDriver = MapDriver.newMapDriver(mapper);
		reduceDriver = ReduceDriver.newReduceDriver(reducer);
	}
	
	@Test
	public void testMapper() throws IOException {
		mapDriver.withInput(new LongWritable(), new Text("P1,P2,P3"));
		mapDriver.withOutput(new ComKey("P1","P2"), new IntWritable(1));
		mapDriver.withOutput(new ComKey("P1","P3"), new IntWritable(1));
		mapDriver.withOutput(new ComKey("P2","P3"), new IntWritable(1));
		mapDriver.runTest();
	}
	
	@Test
	public void testReducer() throws IOException {
		List<IntWritable> values = new ArrayList<IntWritable>();
		values.add(new IntWritable(2));
		values.add(new IntWritable(5));
		reduceDriver.withInput(new ComKey("P1","P2"), values);
		reduceDriver.withOutput(new Text("P1 - P2"), new IntWritable(7));
		reduceDriver.runTest();
	}
}
