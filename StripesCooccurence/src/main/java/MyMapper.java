import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import java.io.*;

public class MyMapper extends Mapper<LongWritable,Text,Text,MapWritable> {
	private int windowSize = 0;
	private MapWritable map = new MapWritable();
	private Text word = new Text();
	
	@Override
	public void setup(Context context) throws IOException, InterruptedException {
		windowSize = context.getConfiguration().getInt("size", 0);
	}
	
	public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
		String[] lst = value.toString().split("\\s+");
		
		if (lst.length > 1) {
			for (int i=0;i<lst.length;i++) {
				map.clear();
				
				for (int j=i - windowSize;j<=i+windowSize;j++) {
					if (j >= 0 && j < lst.length && j != i) {						
						
						Text neighbour = new Text(lst[j]);
						
						if (map.containsKey(neighbour)) {
							IntWritable count = (IntWritable)map.get(neighbour);
							count.set(count.get() + 1);
						}
						else {
							map.put(new Text(lst[j]), new IntWritable(1));
						}
						
					}
				}
				
				word.set(lst[i]);
				context.write(word, map);
			}
		}
	}
}
