import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.io.*;

public class MyPartitioner extends Partitioner<WordPair,IntWritable> {
	public int getPartition(WordPair key, IntWritable value, int numPartitions) {
		return key.getWord().hashCode() % numPartitions;
	}
}
