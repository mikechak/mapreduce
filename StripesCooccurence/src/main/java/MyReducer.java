import java.io.*;
import java.util.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;

public class MyReducer extends Reducer<Text,MapWritable,Text,IntWritable> {
	private Map<String,Integer> map = new HashMap<String,Integer>();
	
	public void reduce(Text key, Iterable<MapWritable> values, Context context) throws IOException, InterruptedException {
				
		for (MapWritable m : values) {
			map.clear();
			
			Set<Writable> keys = m.keySet();
			for (Writable k : keys) {
				IntWritable count = (IntWritable)m.get(k);
				
				if (map.containsKey(k.toString())) {
					map.put(k.toString(), map.get(k.toString()) + count.get());
				}
				else {
					map.put(k.toString(), count.get());
				}
			}
			
			for (Map.Entry<String, Integer> k : map.entrySet()) {
				context.write(new Text(key + " - " + k.getKey()), new IntWritable(k.getValue()));
			}
		}		
		
	}
}

//context.write(new Text(key.getWord().toString() + " - " + key.getNeighbour().toString()), new IntWritable(total));