import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class MyReducer extends Reducer<ComKey, Text, Text, Text> {
				
		public void reduce(ComKey key, Iterable<Text>values, Context context) throws IOException, InterruptedException {
			Map<String,String> map = new HashMap<String,String>();
			String common = "";
			
			for (Text val : values) {
				StringTokenizer itr = new StringTokenizer(val.toString());
				while (itr.hasMoreTokens()) {
					String friendId = itr.nextToken();
					
					if (map.get(friendId) != null) {
						common += friendId + ",";
					}
					else {
						map.put(friendId, "");
					}
				}				
			}
			
			if (common.length() > 1)
			context.write(new Text(key.getUserId().toString() + " + " + key.getFriendId().toString()), new Text(common));
		}
	}
