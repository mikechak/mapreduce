import org.apache.hadoop.io.*;

public class MyGroupingComparator extends WritableComparator {
	public MyGroupingComparator() {
		super(ComKey.class, true);
	}
	
	@Override
	public int compare(WritableComparable a, WritableComparable b) {
		ComKey objA = (ComKey)a;
		ComKey objB = (ComKey)b;
		
		//return objA.getUserId().compareTo(objB.getUserId());
		/*
		int val = objA.getUserId().compareTo(objB.getUserId());
		if (val == 0)
			return objA.getFriendId().compareTo(objB.getFriendId());
		return val;
		*/
		if (objA.getUserId().compareTo(objB.getUserId()) == 0 && objA.getFriendId().compareTo(objB.getFriendId()) == 0) {
			return 0;
		}
		else { 
			return objA.getUserId().compareTo(objB.getUserId());
		}
	}
}
