import java.io.*;
import org.apache.hadoop.io.*;

public class ComKey implements Writable, WritableComparable<ComKey> {
	private Text userId;
	private Text friendId;
	
	public ComKey() {
		userId = new Text();
		friendId = new Text();
	}
	
	public ComKey(String userId, String friendId) {
		//this();
		//set(userId, friendId);
		this.userId = new Text(userId);
		this.friendId = new Text(friendId);
	}
	
	public void set(String userId, String friendId){
		if (userId.compareTo(friendId) < 0) {
			this.userId.set(userId);
			this.friendId.set(friendId);
		}
		else {
			this.userId.set(friendId);
			this.friendId.set(userId);
		}
	}
	
	public Text getUserId() {
		return this.userId;
	}
	
	public Text getFriendId() {
		return this.friendId;
	}
		
	public void write(DataOutput dataOutput) throws IOException {
		this.userId.write(dataOutput);
		this.friendId.write(dataOutput);
	}
	
	public void readFields(DataInput dataInput) throws IOException {
		this.userId.readFields(dataInput);
		this.friendId.readFields(dataInput);
	}
	
	//@Override
	public int compareTo(ComKey obj) {
		int val = this.userId.compareTo(obj.userId);
		if (val == 0) {
			return this.friendId.compareTo(obj.friendId);
		}
		return val;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (this == null || getClass() != obj.getClass()) return false;
		
		ComKey to = (ComKey)obj;
		if (!this.userId.equals(to.userId)) return false;
		if (!this.friendId.equals(to.friendId)) return false;
		
		return true;
	}
	
	@Override
	public int hashCode() {
		return this.userId.hashCode() + this.friendId.hashCode();
	}
}
