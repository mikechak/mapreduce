import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class MyMapper extends Mapper<Text, Text, ComKey, Text> {
		ComKey comKey = new ComKey();
		
		public void map(Text key, Text value, Context context) throws IOException, InterruptedException {
			StringTokenizer itr = new StringTokenizer(value.toString());
			while (itr.hasMoreTokens()) {
				comKey.set(key.toString(), itr.nextToken());
				context.write(comKey, value);
			}
		}
	}
