import java.io.*;
import java.util.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mrunit.mapreduce.*;
import org.junit.Before;
import org.junit.Test;

public class CommonFriendsTest {

	MapDriver<Text,Text,ComKey,Text> mapDriver;
	ReduceDriver<ComKey,Text,Text,Text> reduceDriver;
	MapReduceDriver<Text,Text,ComKey,Text,Text,Text> mapreduceDriver;
	
	@Before
	public void setup() {
		MyMapper mapper = new MyMapper();
		MyReducer reducer = new MyReducer();
		
		mapDriver = MapDriver.newMapDriver(mapper);
		reduceDriver = ReduceDriver.newReduceDriver(reducer);
		mapreduceDriver = MapReduceDriver.newMapReduceDriver(mapper, reducer);
	}
	
	@Test
	public void testMapper() throws IOException {
		mapDriver.withInput(new Text("A"), new Text("B C D"));
		mapDriver.withInput(new Text("B"), new Text("A C D E"));
		mapDriver.withOutput(new ComKey("A", "B"), new Text("B C D"));
		mapDriver.withOutput(new ComKey("A", "C"), new Text("B C D"));
		mapDriver.withOutput(new ComKey("A", "D"), new Text("B C D"));
		mapDriver.withOutput(new ComKey("A", "B"), new Text("A C D E"));
		mapDriver.withOutput(new ComKey("B", "C"), new Text("A C D E"));
		mapDriver.withOutput(new ComKey("B", "D"), new Text("A C D E"));
		mapDriver.withOutput(new ComKey("B", "E"), new Text("A C D E"));
		mapDriver.runTest();
	}
	
	@Test
	public void testReducer() throws IOException {
		List<Text> values = new ArrayList<Text>();
		values.add(new Text("B C D"));
		values.add(new Text("A C D E"));
		reduceDriver.withInput(new ComKey("A", "B"), values);
		reduceDriver.withOutput(new Text("A + B"), new Text("C,D,"));
		reduceDriver.runTest();
	}
	
	@Test
	public void testMapReduce() throws IOException {
		mapreduceDriver.withInput(new Text("A"), new Text("B C D"));
		mapreduceDriver.withInput(new Text("B"), new Text("A C D E"));
		mapreduceDriver.withOutput(new Text("A + B"), new Text("C,D,"));
		mapreduceDriver.runTest();
	}
}
