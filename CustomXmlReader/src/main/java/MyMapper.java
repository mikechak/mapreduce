import java.io.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;

public class MyMapper extends Mapper<LongWritable,Text,NullWritable,Text> {
	public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
		context.write(NullWritable.get(), value);
	}
}
