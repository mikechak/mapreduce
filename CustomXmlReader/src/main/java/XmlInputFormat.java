import java.io.*;
import org.apache.hadoop.mapreduce.lib.input.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;

public class XmlInputFormat extends TextInputFormat {

	@Override
	public RecordReader<LongWritable,Text> createRecordReader(InputSplit split, TaskAttemptContext context) {
		return new XmlRecordReader();
	}
		
}
