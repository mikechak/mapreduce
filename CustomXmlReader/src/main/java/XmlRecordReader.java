import java.io.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.FileSystem;

public class XmlRecordReader extends RecordReader<LongWritable,Text>{
	private LongWritable key = new LongWritable();
	private Text value = new Text();
	private long start;
	private long end;
	private FSDataInputStream fsin;
	private DataOutputBuffer buffer = new DataOutputBuffer();
	private byte[] startTag;
	private byte[] endTag;
	
	@Override
	public LongWritable getCurrentKey() throws IOException, InterruptedException {
		return key;
	}
	
	@Override
	public void initialize(InputSplit is, TaskAttemptContext context) throws IOException, InterruptedException {
		
		startTag = context.getConfiguration().get("startTag").getBytes("utf-8");
		endTag = context.getConfiguration().get("endTag").getBytes("utf-8");
		
		FileSplit fileSplit = (FileSplit)is;
		start = fileSplit.getStart();
		end = start + fileSplit.getLength();
		Path file = fileSplit.getPath();
		FileSystem fs = file.getFileSystem(context.getConfiguration());
		fsin = fs.open(fileSplit.getPath());
		fsin.seek(start);
	}
	
	@Override
	public boolean nextKeyValue() throws IOException, InterruptedException {
		if (fsin.getPos() < end) {
            if (readUntilMatch(startTag, false)) {
                try {
                    buffer.write(startTag);
                    if (readUntilMatch(endTag, true)) {
                        value.set(buffer.getData(), 0, buffer.getLength());
                        key.set(fsin.getPos());
                        return true;
                    }
                } finally {
                    buffer.reset();
                }
            }
        }
		return false;
	}
	
	@Override
	public Text getCurrentValue() throws IOException, InterruptedException {
		return value;
	}
	
	@Override
	public float getProgress() throws IOException, InterruptedException {
		return (fsin.getPos() - start) / (float)(end -start);
	}
	
	@Override
	public void close() throws IOException {
		fsin.close();
	}
	
	private boolean readUntilMatch(byte[] match, boolean withinBlock) throws IOException {
        int i = 0;
        while (true) {
            int b = fsin.read();

            if (b == -1)
                return false;

            if (withinBlock)
                buffer.write(b);

            if (b == match[i]) {
                i++;
                if (i >= match.length)
                    return true;
            } else
                i = 0;

            if (!withinBlock && i == 0 && fsin.getPos() >= end)
                return false;
        }
    }
}
