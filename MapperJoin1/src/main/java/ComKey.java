import org.apache.hadoop.io.*;
import java.io.*;

public class ComKey implements Writable, WritableComparable<ComKey> {
	private Text guid;
	private Text seq;
	
	public ComKey() {
		guid = new Text();
		seq = new Text();
	}
	
	public ComKey(String guid, String seq) {
		this();
		this.guid.set(guid);
		this.seq.set(seq);
	}
	
	public void Set(String guid, String seq) {
		this.guid.set(guid);
		this.seq.set(seq);
	}
	
	public Text getGuid() {
		return this.guid;
	}
	
	public Text getSeq() {
		return this.seq;
	}
	
	public void write(DataOutput dataOutput) throws IOException {
		this.guid.write(dataOutput);
		this.seq.write(dataOutput);
	}
	
	public void readFields(DataInput dataInput) throws IOException {
		this.guid.readFields(dataInput);
		this.seq.readFields(dataInput);
	}
	
	public int hashCode() {
		return this.guid.hashCode() + this.seq.hashCode();
	}
	
	public int compareTo(ComKey obj) {
		int val = this.guid.compareTo(obj.guid);
		if (val == 0) {
			return this.seq.compareTo(obj.seq);
		}
		return val;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null || getClass() != obj.getClass()) return false;
		
		ComKey o = (ComKey)obj;
		if (!this.guid.equals(o.guid)) return false;
		if (!this.seq.equals(o.seq)) return false;
		
		return true;
	}
}
