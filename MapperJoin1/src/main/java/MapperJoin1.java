import org.apache.hadoop.util.*;
import org.apache.hadoop.io.*;

import java.io.File;

import org.apache.hadoop.conf.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.filecache.DistributedCache;
import org.apache.hadoop.mapreduce.lib.input.*;
import org.apache.hadoop.mapreduce.lib.output.*;
import org.apache.hadoop.fs.*;

public class MapperJoin1 extends Configured implements Tool {
	
	public static void main(String[] args) throws Exception {
		int res = ToolRunner.run(new Configuration(), new MapperJoin1(), args);		
		System.exit(res);
	}
	
	public int run(String[] args) throws Exception {
		Configuration conf = new Configuration();
		conf.set("separator", ",");
		conf.setInt("keyIndex", 0);		
		DistributedCache.addCacheFile(new File("input/Emp.txt").toURI(), conf);
		
		Job job = Job.getInstance(conf);
		job.setJarByClass(MapperJoin1.class);
		
		job.setMapperClass(MyMapper.class);
		
		job.setOutputKeyClass(NullWritable.class);
		job.setOutputValueClass(Text.class);
		
		FileInputFormat.addInputPath(job, new Path(args[0]));
		job.setInputFormatClass(TextInputFormat.class);
		
		FileOutputFormat.setOutputPath(job, new Path(args[1]));
		job.setOutputFormatClass(TextOutputFormat.class);
		
		return job.waitForCompletion(true) ? 1 : 0;
	}
}
