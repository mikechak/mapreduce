import java.io.*;
import java.util.*;
import java.net.URI;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.google.common.base.Joiner;

public class MyMapper extends Mapper<LongWritable,Text,NullWritable,Text> 
{
	Map<String,String> map = new HashMap<String,String>();
	String separator;
	int keyIndex;
	Splitter splitter;
	Joiner joiner;
	String k;
	String v;
	ComKey comKey = new ComKey();
	
	@Override
	public void setup(Context context) throws IOException {
		separator = context.getConfiguration().get("separator");
		keyIndex = context.getConfiguration().getInt("index", 0);
		
		splitter = Splitter.on(separator);
		joiner = Joiner.on(separator);
		
		URI[] uri = context.getCacheFiles();			
		
		Path path = new Path(uri[0].toString());
		FileSystem fs = path.getFileSystem(context.getConfiguration());
		BufferedReader br = new BufferedReader(new InputStreamReader(fs.open(path)));
		
		String line;
		line = br.readLine();		
		while (line != null) {
			//System.out.println(line);
			List<String> tmp = Lists.newArrayList(splitter.split(line));
			k = tmp.remove(keyIndex);
			v = joiner.join(tmp);
			if (map.get(k) == null) {
				map.put(k, v);
			}
			
			line = br.readLine();
		}
	}
	
	public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException
	{
		List<String> tmp = Lists.newArrayList(splitter.split(value.toString()));
		String guid = tmp.remove(keyIndex);
		
		if (map.get(guid) != null) {
			context.write(NullWritable.get(), new Text(guid + separator + joiner.join(tmp) + separator + map.get(guid)));
		}
		
	}
}
