import org.apache.hadoop.mrunit.mapreduce.*;
import java.io.File;
import java.io.*;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.filecache.DistributedCache;
import org.junit.Before;
import org.junit.Test;

public class MapperJoin1Test {
	MapDriver<LongWritable,Text,NullWritable,Text> mapDriver;
	
	@Before
	public void setup() {
		MyMapper mapper = new MyMapper();
		
		mapDriver = MapDriver.newMapDriver(mapper);
	}
	
	@Test
	public void testMapper() throws IOException {
		Configuration conf = mapDriver.getConfiguration();
		conf.set("separator", ",");
		conf.setInt("keyIndex", 0);
		
		DistributedCache.addCacheFile(new File("input/Emp.txt").toURI(), conf);
		
		mapDriver.withInput(new LongWritable(), new Text("08db7c55-22ae-4199-8826-c67a5689f838,John,Gregory,258 Khale Street,Florence,SC"));
		mapDriver.withOutput(NullWritable.get(), new Text("08db7c55-22ae-4199-8826-c67a5689f838,John,Gregory,258 Khale Street,Florence,SC,Ellman's Catalog Showrooms"));
		mapDriver.runTest();
	}
}
