import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mrunit.mapreduce.*;
import org.junit.Before;
import org.junit.Test;
import java.io.*;
import java.util.*;

public class MovingAverageTest {
	MapDriver<LongWritable,Text,ComKey,DoubleWritable> mapDriver;
	ReduceDriver<ComKey,DoubleWritable,Text,DoubleWritable> reduceDriver;
	
	@Before
	public void setup() {
		MyMapper mapper = new MyMapper();
		MyReducer reducer = new MyReducer();
		
		mapDriver = MapDriver.newMapDriver(mapper);
		reduceDriver = ReduceDriver.newReduceDriver(reducer);
	}
	
	@Test
	public void testMapper() throws IOException {
		mapDriver.withInput(new LongWritable(), new Text("GOOG,2004-11-04,184.70"));
		mapDriver.withOutput(new ComKey("GOOG","2004-11-04"), new DoubleWritable(184.70));
		mapDriver.runTest();
	}
	
	@Test
	public void testReducer() throws IOException {
		Configuration conf = reduceDriver.getConfiguration();
		conf.setInt("period", 2);
		
		List<DoubleWritable> values = new ArrayList<DoubleWritable>();
		values.add(new DoubleWritable(194.87));		
		reduceDriver.withInput(new ComKey("GOOG", "2004-11-02"), values);
		
		values = new ArrayList<DoubleWritable>();
		values.add(new DoubleWritable(191.67));
		reduceDriver.withInput(new ComKey("GOOG", "2004-11-03"), values);
		
		values = new ArrayList<DoubleWritable>();
		values.add(new DoubleWritable(184.70));
		reduceDriver.withInput(new ComKey("GOOG", "2004-11-04"), values);
		
		values = new ArrayList<DoubleWritable>();
		values.add(new DoubleWritable(918.55));
		reduceDriver.withInput(new ComKey("GOOG", "2013-07-17"), values);
		
		values = new ArrayList<DoubleWritable>();
		values.add(new DoubleWritable(910.68));
		reduceDriver.withInput(new ComKey("GOOG", "2013-07-18"), values);
		
		values = new ArrayList<DoubleWritable>();
		values.add(new DoubleWritable(896.60));
		reduceDriver.withInput(new ComKey("GOOG", "2013-07-19"), values);
		
		reduceDriver.withOutput(new Text("GOOG 2004-11-03"), new DoubleWritable(193.26999999999998));
		reduceDriver.withOutput(new Text("GOOG 2004-11-04"), new DoubleWritable(188.18499999999997));
		reduceDriver.withOutput(new Text("GOOG 2013-07-17"), new DoubleWritable(551.625));
		reduceDriver.withOutput(new Text("GOOG 2013-07-18"), new DoubleWritable(914.615));
		reduceDriver.withOutput(new Text("GOOG 2013-07-19"), new DoubleWritable(903.6400000000001));
		
		reduceDriver.runTest();
	}
}
