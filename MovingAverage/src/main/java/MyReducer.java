import java.io.*;
import java.util.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.io.*;

public class MyReducer extends Reducer<ComKey,DoubleWritable,Text,DoubleWritable> {

	private int period;
	private double[] window;
	private int pointer = 0;
	private double sum = 0;
	private boolean full = false;
	
	@Override
	public void setup(Context context) {
		period = context.getConfiguration().getInt("period", 0);
		window = new double[period];
	}
	
	public void reduce(ComKey key, Iterable<DoubleWritable> values, Context context) throws IOException, InterruptedException {
		for (DoubleWritable val : values) {
			/*
			window[pointer] = val.get();
			sum += window[pointer];						
			if (pointer + 1 == period || full) {
				context.write(new Text(key.getCompany()+" "+key.getDate()), new DoubleWritable(getMovingAverage()));
				pointer = 0;
				sum -= window[pointer];
				full = true;
			}
			else {
				pointer++;
			}*/
			window[pointer % period] = val.get();
			sum += val.get();
			pointer++;
			
			if (!full && pointer % period == 0) full = true;
									
			if (full) {
				context.write(new Text(key.getCompany()+" "+key.getDate()), new DoubleWritable(getMovingAverage()));
				sum -= window[pointer % period];
			}
			
			//pointer++;
		}
	}
	
	public double getMovingAverage() {
		return sum/period;
	}
}
