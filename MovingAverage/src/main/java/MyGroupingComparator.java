import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.io.*;

public class MyGroupingComparator extends WritableComparator {
	public MyGroupingComparator() {
		super(ComKey.class, true);
	}
	
	@Override //this Override annotation is very important
	public int compare(WritableComparable objA, WritableComparable objB) {
		ComKey a = (ComKey)objA;
		ComKey b = (ComKey)objB;
		
		return a.getCompany().compareTo(b.getCompany());
	}
}
