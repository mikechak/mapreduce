import org.apache.hadoop.io.*;
import java.io.*;

public class ComKey implements Writable, WritableComparable<ComKey> {
	private Text company;
	private Text date;
	
	public ComKey() {
		company = new Text();
		date = new Text();
	}
	
	public ComKey(String co, String dt) {
		this();
		company.set(co);
		date.set(dt);
	}
	
	public Text getCompany() {
		return company;
	}
	
	public Text getDate() {
		return date;
	}
	
	public void write(DataOutput dataOutput) throws IOException {
		this.company.write(dataOutput);
		this.date.write(dataOutput);
	}
	
	public void readFields(DataInput dataInput) throws IOException {
		this.company.readFields(dataInput);
		this.date.readFields(dataInput);
	}
	
	public int compareTo(ComKey obj) {
		int val = this.company.compareTo(obj.company);
		if (val == 0) {
			return this.date.compareTo(obj.date);
		}
		return val;
	}
	
	public boolean equals(Object obj) {
		if (obj == this) return true;
		if (obj == null || obj.getClass() != getClass()) return false;
		
		ComKey a = (ComKey)obj;
		if (!company.equals(a.company)) return false;
		if (!date.equals(a.date)) return false;
		
		return true;
	}
	
	public int hashCode() {
		return company.hashCode() + date.hashCode();				
	}
}
