import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.io.*;

public class MyCustomPartitioner extends Partitioner<ComKey,Text> {
	public int getPartition(ComKey key, Text value, int partitionNum) {
		return key.getCompany().hashCode() % partitionNum;
	}
}
