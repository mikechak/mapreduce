import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.io.*;

//import java.awt.List;
import java.io.*;
import java.util.Arrays;
import java.util.*;

public class MyMapper extends Mapper<LongWritable,Text,ComKey,DoubleWritable> {
	private ComKey cKey = new ComKey();
	
	public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
		List<String> lst = Arrays.asList(value.toString().split(","));
		
		cKey = new ComKey(lst.get(0),lst.get(1));
		
		context.write(cKey,new DoubleWritable(Double.parseDouble(lst.get(2))));
	}
}
