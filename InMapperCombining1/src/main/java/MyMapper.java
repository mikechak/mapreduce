import java.io.*;
import java.util.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.io.*;

public class MyMapper extends Mapper<LongWritable,Text,Text,IntWritable> {
	private Map<String,Integer> map = new HashMap<String,Integer>();
		
	public void setup(Context context) {
		
	}
	
	public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
		StringTokenizer itr = new StringTokenizer(value.toString());
		String token;
		
		while (itr.hasMoreTokens()) {
			token = itr.nextToken().toLowerCase();
			if (!map.containsKey(token)) map.put(token, 0);
			//Integer count = map.get(token) + 1;
			map.put(token, map.get(token)+1);
			//map.put(token, count);
		}
	}
	
	public void cleanup(Context context) throws IOException, InterruptedException {
		for (Map.Entry<String, Integer> entry : map.entrySet()) {
			context.write(new Text(entry.getKey()), new IntWritable(entry.getValue()));
		}
	}

}
