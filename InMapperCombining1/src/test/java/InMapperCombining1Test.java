import java.io.*;
import java.util.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mrunit.mapreduce.*;
import org.junit.Before;
import org.junit.Test;

public class InMapperCombining1Test {
	MapDriver<LongWritable,Text,Text,IntWritable> mapDriver;
	
	@Before
	public void setup()
	{
		MyMapper mapper = new MyMapper();
		
		mapDriver = MapDriver.newMapDriver(mapper);
	}
	
	@Test
	public void testMapper() throws IOException {
		mapDriver.withInput(new LongWritable(), new Text("The fox jump over the fox"));		
		mapDriver.withOutput(new Text("the"), new IntWritable(2));
		mapDriver.withOutput(new Text("over"), new IntWritable(1));
		mapDriver.withOutput(new Text("fox"), new IntWritable(2));
		mapDriver.withOutput(new Text("jump"), new IntWritable(1));
		
		
		mapDriver.runTest();
	}
}
