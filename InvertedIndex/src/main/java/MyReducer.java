import java.util.*;
import java.io.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.io.*;

public class MyReducer extends Reducer<Text,Text,Text,Text> {
	private StringBuilder sb = new StringBuilder();
	private Text index = new Text();
	
	public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException 
	{
		sb = new StringBuilder();
		index.set(key);
		
		for (Text val : values) {
			sb.append(val);
			sb.append(",");
		}
		
		context.write(index, new Text(sb.toString()));
	}
	
	/*
	@Override
	public void cleanup(Context context) throws IOException, InterruptedException
	{
		sb.setLength(sb.length()-1);
		context.write(index, new Text(sb.toString()));
	}*/
}
