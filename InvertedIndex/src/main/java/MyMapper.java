import java.io.*;
import java.util.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.*;

public class MyMapper extends Mapper<LongWritable,Text,Text,Text> {
	private String filename;
	private Text word = new Text();
	
	public void setup(Context context) {
		Configuration conf = context.getConfiguration();
		FileSplit sf = (FileSplit)context.getInputSplit();
		filename = sf.getPath().getName();
	}
	
	public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException
	{
		StringTokenizer itr = new StringTokenizer(value.toString());
		
		while (itr.hasMoreTokens()) {
			word.set(itr.nextToken());
			context.write(word, new Text(filename));
		}
	}

}
