import java.util.*;
import java.io.*;
import org.apache.hadoop.mrunit.mapreduce.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.*;
import org.junit.Before;
import org.junit.Test;

public class InvertedIndexTest {
	MapDriver<LongWritable,Text,Text,Text> mapDriver;
	ReduceDriver<Text,Text,Text,Text> reduceDriver;
	MapReduceDriver<LongWritable,Text,Text,Text,Text,Text> mapreduceDriver;
	
	@Before
	public void setup() {
		MyMapper mapper = new MyMapper();
		MyReducer reducer = new MyReducer();
		
		mapDriver = MapDriver.newMapDriver(mapper);
		reduceDriver = ReduceDriver.newReduceDriver(reducer);
		mapreduceDriver = MapReduceDriver.newMapReduceDriver(mapper,reducer);
	}
	
	@Test
	public void testMapper() throws IOException {
		Path path = new Path("index.txt");
		mapDriver.setMapInputPath(path);
		
		mapDriver.withInput(new LongWritable(), new Text("Attempting graceful shutdown of VM"));
		mapDriver.withOutput(new Text("Attempting"), new Text("index.txt"));
		mapDriver.withOutput(new Text("graceful"), new Text("index.txt"));
		mapDriver.withOutput(new Text("shutdown"), new Text("index.txt"));
		mapDriver.withOutput(new Text("of"), new Text("index.txt"));
		mapDriver.withOutput(new Text("VM"), new Text("index.txt"));
		mapDriver.runTest();
	}
	
	@Test
	public void testReducer() throws IOException {
		List<Text> values = new ArrayList<Text>();
		values.add(new Text("index.txt"));
		values.add(new Text("doc.txt"));
		
		reduceDriver.withInput(new Text("Attempting"), values);
		reduceDriver.withOutput(new Text("Attempting"), new Text("index.txt,doc.txt"));
		reduceDriver.runTest();
	}
}
