import java.io.*;
import java.util.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mrunit.mapreduce.*;
import org.apache.hadoop.conf.*;
import org.junit.Before;
import org.junit.Test;

public class PairsCooccurenceTest {
	MapDriver<LongWritable,Text,WordPair,IntWritable> mapDriver;
	ReduceDriver<WordPair,IntWritable,Text,IntWritable> reduceDriver;
	
	@Before
	public void setup() {
		MyMapper mapper = new MyMapper();
		MyReducer reducer = new MyReducer();
		
		mapDriver = MapDriver.newMapDriver(mapper);
		reduceDriver = ReduceDriver.newReduceDriver(reducer);
	}
	
	@Test
	public void testMapper() throws IOException {
		Configuration conf = mapDriver.getConfiguration();
		conf.setInt("size", 2);
		
		mapDriver.withInput(new LongWritable(), new Text("A B C"));
		//List<IntWritable> values = new ArrayList<IntWritable>();
		//values.add(new IntWritable(1));
		mapDriver.withOutput(new WordPair("A","B"), new IntWritable(1));
		mapDriver.withOutput(new WordPair("A","C"), new IntWritable(1));
		mapDriver.withOutput(new WordPair("B","A"), new IntWritable(1));
		mapDriver.withOutput(new WordPair("B","C"), new IntWritable(1));
		mapDriver.withOutput(new WordPair("C","A"), new IntWritable(1));
		mapDriver.withOutput(new WordPair("C","B"), new IntWritable(1));
		
		mapDriver.runTest();		
	}
}
