import org.apache.hadoop.io.*;
import java.io.*;

public class WordPair implements Writable, WritableComparable<WordPair> {
	private Text word;
	private Text neighbour;
	
	public WordPair() {
		word = new Text();
		neighbour = new Text();
	}
	
	public WordPair(String word, String neighbour) {
		this();
		this.word.set(word);
		this.neighbour.set(neighbour);
	}
	
	public void setWord(String word) {
		this.word.set(word);
	}
	
	public void setNeighbour(String neighbour) {
		this.neighbour.set(neighbour);
	}
	
	public Text getWord() {
		return word;
	}
	
	public Text getNeighbour() {
		return neighbour;
	}
	
	public void write(DataOutput dataOutput) throws IOException {
		this.word.write(dataOutput);
		this.neighbour.write(dataOutput);
	}
	
	public void readFields(DataInput dataInput) throws IOException {
		this.word.readFields(dataInput);
		this.neighbour.readFields(dataInput);
	}
	
	public int compareTo(WordPair obj) {
		int val = this.word.compareTo(obj.word);
		if (val == 0) {
			return this.neighbour.compareTo(obj.neighbour);
		}
		return val;
	}
	
	public boolean equals(Object obj) {
		if (obj == this) return true;
		if (obj == null || obj.getClass() != getClass()) return false;
		
		WordPair wp = (WordPair)obj;
		if (this.word != wp.word) return false;
		if (this.neighbour != wp.neighbour) return false;
		
		return true;
	}
	
	public int hashCode() {
		return word.hashCode() + neighbour.hashCode();
	}

}
