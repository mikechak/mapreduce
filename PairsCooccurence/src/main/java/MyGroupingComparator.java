import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;

public class MyGroupingComparator extends WritableComparator {
	public MyGroupingComparator() {
		super(WordPair.class, true);		
	}
	
	@Override
	public int compare(WritableComparable objA, WritableComparable objB) {
		WordPair a = (WordPair)objA;
		WordPair b = (WordPair)objB;
		
		int val = a.getWord().compareTo(b.getWord());
		if (val == 0) {
			return a.getNeighbour().compareTo(b.getNeighbour());
		}
		return val;
		
		//return a.getWord().compareTo(b.getWord());
	}
}
