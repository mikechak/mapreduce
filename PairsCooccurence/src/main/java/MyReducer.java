import java.io.*;
import java.util.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;

public class MyReducer extends Reducer<WordPair,IntWritable,Text,IntWritable> {
	int total;
	
	public void reduce(WordPair key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
		total = 0;
		
		for (IntWritable val : values) {
			total += val.get();
		}
		context.write(new Text(key.getWord().toString() + " - " + key.getNeighbour().toString()), new IntWritable(total));
	}
}
