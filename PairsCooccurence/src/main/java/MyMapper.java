import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import java.io.*;

public class MyMapper extends Mapper<LongWritable,Text,WordPair,IntWritable> {
	private int windowSize = 0;
	private IntWritable ONE = new IntWritable(1);
	private WordPair wordPair = new WordPair();
	
	@Override
	public void setup(Context context) throws IOException, InterruptedException {
		windowSize = context.getConfiguration().getInt("size", 0);
	}
	
	public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
		String[] lst = value.toString().split("\\s+");
		
		if (lst.length > 1) {
			for (int i=0;i<lst.length;i++) {
				for (int j=i - windowSize;j<=i+windowSize;j++) {
					if (j >= 0 && j < lst.length && j != i) {						
						wordPair.setWord(lst[i]);
						wordPair.setNeighbour(lst[j]);
						context.write(wordPair, ONE);
					}
				}
			}
		}
	}
}
