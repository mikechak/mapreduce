import java.io.*;
import java.util.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mrunit.mapreduce.*;
import org.junit.Before;
import org.junit.Test;
//import static WordCount1.MyMapper;
//import WordCount1.MyReducer;

public class WordCount1Test {
	MapDriver<LongWritable, Text, Text, IntWritable> mapDriver;
	ReduceDriver<Text, IntWritable, Text, IntWritable> reduceDriver;
	MapReduceDriver<LongWritable, Text, Text, IntWritable, Text, IntWritable> mapreduceDriver;
	
	@Before
	public void setup() {
		WordCount1.MyMapper mapper = new WordCount1.MyMapper();
		WordCount1.MyReducer reducer = new WordCount1.MyReducer();
		mapDriver = MapDriver.newMapDriver(mapper);
		reduceDriver = ReduceDriver.newReduceDriver(reducer);
		mapreduceDriver = MapReduceDriver.newMapReduceDriver(mapper, reducer);
	}
	
	@Test
	public void testMapper() throws IOException
	{
		mapDriver.withInput(new LongWritable(), new Text("Man Utd"));
		mapDriver.withOutput(new Text("Man"), new IntWritable(1));
		mapDriver.withOutput(new Text("Utd"), new IntWritable(1));
		mapDriver.runTest();
	}
	
	@Test
	public void testReducer() throws IOException
	{
		List<IntWritable> values = new ArrayList<IntWritable>();
		values.add(new IntWritable(1));
		values.add(new IntWritable(1));
		reduceDriver.withInput(new Text("Man"), values);
		reduceDriver.withOutput(new Text("Man"), new IntWritable(2));
		reduceDriver.runTest();
	}
	
	@Test
	public void testMapReduce() throws IOException
	{
		mapreduceDriver.withInput(new LongWritable(), new Text("Man Utd"));
		List<IntWritable> values = new ArrayList<IntWritable>();
		values.add(new IntWritable(1));
		values.add(new IntWritable(1));
		mapreduceDriver.withOutput(new Text("Man"), new IntWritable(1));
		mapreduceDriver.withOutput(new Text("Utd"), new IntWritable(1));
		mapreduceDriver.runTest();
	}
}
